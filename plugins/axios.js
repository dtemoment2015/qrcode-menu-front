
let s4 = () => {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
}

export default ({ $axios, app }) => {

    if (app.$cookiz.get('token')) {
        $axios.setToken(app.$cookiz.get('token'), 'Bearer');
    }

    if (!app.$cookiz.get('device_id')) {
        app.$cookiz.set('device_id',
            s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4(),
            {
                maxAge: 60 * 60 * 24 * 365,
                path: "/",
            });
    }

    if (app.$cookiz.get('token')) {
        $axios.setToken(app.$cookiz.get('token'), 'Bearer');
    }
    
    if (app.$cookiz.get('lang')) {
        $axios.setHeader('localization',app.$cookiz.get('lang'));
    }

    if (app.$cookiz.get('device_id')) {
        $axios.setHeader('device_id', app.$cookiz.get('device_id'));
    }

    $axios.interceptors.request.use((config) => {
        if (app.store) {
            app.store.dispatch('initLoading', true);
        }
        return config;
    }, (error) => {
        return Promise.reject(error);
    });

    $axios.interceptors.response.use(
        (response) => {

            if (app.store) {
                app.store.dispatch('initLoading', false);
            }

            return response;
        },
        (error) => {
            if (error.response.status == '401') {

            }
            if (error.response.status == '429') {
                if (window && window.location) {
                    alert('Вы делаете слишком много запросов в систему. Авторизуйтесь позже.')
                    window.location.href = "/";
                }
            }
            if (error.response.status == '500') {

            }
            if (error.response.status == '422') {

            }




            if (app.store) {
                app.store.dispatch('initLoading', false);
            }


            return Promise.reject(error);
        }
    );
}