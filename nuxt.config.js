export default {
  server: {
    port: 3000, // default: 3000
    host: '0.0.0.0', // default: localhost
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'EZZY',
    meta: [
      {
        name: 'yandex-verification',
        content: '3ffbdfc9b7470fa7'
     },
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1.0,  maximum-scale=1.0, user-scalable=0'
      },
      { hid: 'description', name: 'description', content: 'QR MENU' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: 'http://api.ezzy.uz/img/favicon.png' }],
  },

     router: {
        extendRoutes(routes, resolve) {
            routes.push({
                name: 'Not Found',
                path: '*',
                component: resolve(__dirname, 'pages/404.vue')
            })
        }
    },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    {
      src: '~assets/css/reset.css',
      lang: 'css'
    },
    {
      rel: "prelaod",
      src: '~assets/fonts/icons/style.css'
    },
    {
      src: '~assets/sass/main.scss',
      lang: 'scss'
    },
  ],

  loadingIndicator: {
    name: 'circle',
    color: '#3468EF',
    background: 'white'
  },
  loading: { color: '#3468EF', height: '4px', failedColor: 'red' },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/ob.js', mode: 'client' },
    { src: '~/plugins/tooltip.js', mode: 'client' },
    { src: '~/plugins/filters.js', ssr: true },
    { src: '~/plugins/axios.js', ssr: true },
    { src: '~/plugins/toastr.js', mode: 'client' },
    { src: '~/plugins/markdown.js', mode: 'client', ssr: false },
    { src: '~/plugins/qrcode.js', mode: 'client', ssr: false },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [{ path: '~/components', pathPrefix: false }],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/device',
    // '@nuxtjs/eslint-module',

    // '@nuxtjs/stylelint-module',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    ['cookie-universal-nuxt', { alias: 'cookiz', parseJSON: true }],
    [
      '@nuxtjs/yandex-metrika',
      {
          id: '83183533',
          webvisor: true,
      }
  ],
    '@nuxtjs/robots',
  ],

  robots: {
    UserAgent: '*',
    Disallow: ['/'],
    // Sitemap: 'https://arenamarket.uz/sitemap.xml',
},

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    //  baseURL: 'http://api.ezzy.uz/api/v1/',
    baseURL: 'http://127.0.0.1:8002/api/v1/',
    withCredentials: true
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extractCSS: true,
    optimization: {
      splitChunks: {
        cacheGroups: {
          styles: {
            name: 'styles',
            test: /\.(css|vue)$/,
            chunks: 'all',
            enforce: true
          }
        }
      }
    },
  },
}
