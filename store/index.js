import apiAccount from "~/_api/apiAccount";
import apiFont from "~/_api/apiFont";
import apiLocale from "~/_api/apiLocale";

export const state = () => ({
    profile: null,
    loading: false,
    localizations: {},
    fonts: [],
})

export const mutations = {
    SET_PROFILE(state, payload) {
        state.profile = payload;
    },
    SET_LOADING(state, payload) {
        state.loading = payload;
    },
    SET_LOCALIZATION(state, payload) {
        state.localizations = payload;
    },
    SET_FONT(state, payload) {
        state.fonts = payload;
    }
}

export const actions = {

    initProfile({ commit }, payload) {
        commit('SET_PROFILE', payload);
    },
    initLocalization({ commit }, payload) {
        commit('SET_LOCALIZATION', payload);
    },
    initFont({ commit }, payload) {
        commit('SET_FONT', payload);
    },
    initLoading({ commit }, payload) {
        commit('SET_LOADING', payload);
    },
    async nuxtServerInit({ dispatch }, { app, $axios, store, redirect }) {
        let profile = null;
        if (app.$cookiz.get('token')) {
            profile = await apiAccount($axios, store, app.$cookiz);
            dispatch('initProfile', profile);
        }
        dispatch('initLocalization', await apiLocale($axios));
        dispatch('initFont', await apiFont($axios));
    }
}