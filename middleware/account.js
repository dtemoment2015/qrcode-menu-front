export default function (context) {
   let { app, redirect } = context;
   if (!app.store.state.profile) {
      redirect("/");
   }
}