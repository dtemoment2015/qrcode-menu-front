import { catchNotify } from './catchNotify'

export async function set(type = 'update', data = {}, method = 'put') {
   // this.$vToastify.loader("Подождите, применяем данные", "Меню");
   return this.$axios({
      method: method,
      url: `business/menu/${type}`,
      data: data
   }).then(res => {
      if (type === 'clone' || type === 'create') {
         this.$router.push({ path: `/account/menu/${res.data.id}` });
      }
      if (type === 'delete' && this.menus && this.menus.data) {

         for (let i = 0; i < this.menus.data.length; i++) {
            if (this.menus.data[i].id == data.id) {
               this.menus.data.splice(i, 1);
               break;
            }
         }
      }
      // this.$vToastify.success("Данные успешно установлены", "Меню");
   }).catch(error => {
      if (error.response) {
         catchNotify(this.$vToastify, error.response.data);
      }
   }).finally(() => {

      this.$vToastify.stopLoader();
   });
};

export function deleteItem(menu) {
   this.$vToastify
      .prompt({
         title: 'Удаление меню',
         body: 'Вы точно хотите удалить меню?',
         answers: { Да: true, Отмена: false },
      })
      .then((value) => {
         if (value === true) {
            this.set('delete', { id: menu.id }, 'delete');
         }
      })
   return false

}

export async function cloneMenu(id, fromLang, newLang) {
   this.$vToastify.loader("Подождите, мы делаем клонирование языка", " Меню");
   return this.$axios({
      method: 'post',
      url: `business/menu/clone`,
      data: {
         id: id,
         from_lang: fromLang,
         new_lang: newLang
      }
   }).then(res => {
      window.location.href = `/account/menu/${id}?language=${newLang}`;
   }).catch(error => {
      if (error.response) {
         catchNotify(this.$vToastify, error.response.data);
      }
   }).finally(() => {
      this.$vToastify.stopLoader();
   });
};
export async function rejectMenu(id, locale) {

   this.$vToastify
   .prompt({
      title: 'Удаление меню',
      body: 'Вы точно хотите удалить этот язык из меню меню?',
      answers: { Да: true, Отмена: false },
   })
   .then((value) => {
      if (value === true) {
         this.$vToastify.loader("Подождите, мы удаляем язык", "Удаление языка");
         return this.$axios({
            method: 'delete',
            url: `business/menu/reject`,
            data: {
               id: id,
               locale: locale
            }
         }).then(res => {
            window.location.href = `/account/menu/${id}`;
         }).catch(error => {
            if (error.response) {
               catchNotify(this.$vToastify, error.response.data);
            }
         }).finally(() => {
            this.$vToastify.stopLoader();
         });
      }
   })
return false

  
};

