import { catchNotify } from './catchNotify'

export async function subscribe(data = {}) {
   this.$vToastify.loader("Подождите, идет процесс записи", "Email");
   return this.$axios({
      method: 'post',
      url: `project/subscribe`,
      data: data
   }).then(res => {
      this.$vToastify.success("Вы успешно подписались на новости QRMENU", "Email");
      this.data.email = '';
   }).catch(error => {
      if (error.response) {
         catchNotify(this.$vToastify, error.response.data);
      }
   }).finally(() => {
      this.$vToastify.stopLoader();
   });
};

