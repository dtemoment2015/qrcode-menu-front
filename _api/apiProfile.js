import { catchNotify } from './catchNotify'

export async function update(data = {}) {
   this.$vToastify.loader("Подождите, обновляем данные", "Профиль");
   return this.$axios({
      method: 'put',
      url: `business/update`,
      data: data
   }).then(res => {
      this.$vToastify.success("Данные успешно обновлены", "Профиль");
   }).catch(error => {
      if (error.response) {
         catchNotify(this.$vToastify, error.response.data);
      }
   }).finally(() => {
      this.$vToastify.stopLoader();
   });
};

