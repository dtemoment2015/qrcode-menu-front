const apiFont = (axios) => {
   return axios({
      method: 'get',
      url: `project/fonts`
   }).then(res => {
      return res.data.data;
   }).catch(() => {
      return null;
   })
}

export default apiFont