const apiMenuClientPreview = (axios, params = {}) => {
   return axios({
      method: 'get',
      params: params,
      url: `client/menu/preview`
   }).then(res => {
      return res.data;
   }).catch(() => {
      redirect('/');
      return null;
   })
}

export default apiMenuClientPreview