const apiBusiness = (axios, params = {}, method = '', redirect) => {
   return axios({
      method: 'get',
      params: params,
      url: `business/businesses/${method}`
   }).then(res => {
      return res.data;
   }).catch(() => {
      redirect('/');
      return null;
   })
}

export default apiBusiness