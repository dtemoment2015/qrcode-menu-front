import { catchNotify } from './catchNotify'


export async function apiRegister(params) {

   let id = this.$vToastify.loader("Подождите, мы проверяем ваши данные", "Регистрация");
   return this.$axios({
      method: 'post',
      url: `auth/register`,
      data: params
   }).then(res => {
      initUser(this, res);
   }).catch(error => {
      if (error.response) {
         catchNotify(this.$vToastify, error.response.data);
      }
   }).finally(() => {
      this.$vToastify.stopLoader();
   });
};

export async function apiRestore(params) {
   this.$vToastify.loader("Подождите, мы проверяем ваши данные", "Авторизация");
   return await this.$axios({
      method: 'post',
      url: `auth/restore`,
      data: params
   }).then(res => {
      this.$emit('input', 'login')
   }).catch(error => {
      if (error.response) {
         catchNotify(this.$vToastify, error.response.data);
      }
   }).finally(() => {
      this.$vToastify.stopLoader();
   });
};

export async function apiCode(params, isRestore = false) {
   params['is_restore'] = isRestore;
   this.$vToastify.loader("Подождите, идет отправка смс", "СМС");
   return await this.$axios({
      method: 'post',
      url: `auth/code`,
      data: params
   }).then(res => {
         this.$emit('input',params['phone']);
   }).catch(error => {
      this.$emit('input',null);
      if (error.response) {
         catchNotify(this.$vToastify, error.response.data);
      }
   }).finally(() => {
      this.$vToastify.stopLoader();
   });
};

export async function apiVerify(params) {
   this.$vToastify.loader("Подождите, мы проверяем ваши данные", "Проверка");
   return await this.$axios({
      method: 'post',
      url: `auth/verify`,
      data: params
   }).then(res => {
      this.$emit('input',res.data.key);
   }).catch(error => {
      this.$emit('input',null);
      if (error.response) {
         catchNotify(this.$vToastify, error.response.data);
      }
   }).finally(() => {
      this.$vToastify.stopLoader();
   });
};

export async function apiLogin(params) {

   this.$vToastify.loader("Подождите, мы проверяем ваши данные", "Авторизация");
   return await this.$axios({
      method: 'post',
      url: `auth/login`,
      data: params
   }).then(res => {
      initUser(this, res, params);
   }).catch(error => {
      if (error.response) {
         catchNotify(this.$vToastify, error.response.data);
      }
   }).finally(() => {
      this.$vToastify.stopLoader();
   });
};

async function initUser(app, res) {

   let id = app.$vToastify.loader("Подождите, мы проверяем ваши данные", "Авторизация");
   const date = new Date(res.data.access.token.expires_at);
   const expires_at = Math.floor(date / 1000);
   app.$cookiz.set('token', res.data.access.accessToken, {
      maxAge: expires_at,
      path: '/'
   });
   app.$axios.setToken(res.data.access.accessToken, 'Bearer');
   await app.$store.dispatch('initProfile', res.data);
   app.$vToastify.stopLoader();
   app.$router.push({ path: '/account' });
}

export function logoutUser() {

   this.$vToastify.prompt({
      title: "Профиль",
      body: "Вы точно хотите выйти?",
      answers: { 'Да': true, 'Отмена': false }
   }
   ).then(value => {
      if (value === true) {
         this.$vToastify.loader("Подождите, мы проверяем ваши данные", "Выход из системы");
         this.$cookiz.remove('token');
         this.$axios.setToken(false);
         this.$store.dispatch('initProfile', null);
         this.$router.push({ path: '/' });
         this.$vToastify.stopLoader();
      }
   });

   return false;

}