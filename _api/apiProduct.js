import { catchNotify } from './catchNotify'

export async function apiProduct(data = {}, type = 'create', method = 'post', params = {}) {

   // this.$vToastify.loader("Подождите, загрузка данных", "Продукция");

   return this.$axios({
      method: method,
      url: `business/menu/category/product/${type}`,
      data: data,
      params: params
   }).then(res => {
      if (type != '') {
         // this.$vToastify.success("Данные успешно применены", "Продукция");
      }


      if (type === 'create') {
         if (this.data) {
            this.data.push(res.data);
         }
         if (this.isCreate) {
            this.isCreate = false;
         }
      }


      return res.data;
   }).catch(error => {
      if (error.response) {
         catchNotify(this.$vToastify, error.response.data);
      }
   }).finally(() => {
      if (this.loading) {
         this.loading = false;
      }
      // this.$vToastify.stopLoader();
   });
};

export async function apiCategory(data = {}, type = 'create', method = 'post', params = {}) {
   // this.$vToastify.loader("Подождите, обновляем данные", "Категория");
   return this.$axios({
      method: method,
      url: `business/menu/category/${type}`,
      data: data,
      params: params
   }).then(res => {
      data = {};

      if (type === 'create') {
         if (this.data) {
            this.data.push(res.data);
            this.$router.push({ query: { ...this.$route.query,category_id: res.data.id } });
         }
         if (this.isCreate) {
            this.isCreate = false;
         }
      }
      return res.data;
   }).catch(error => {
      if (error.response) {
         catchNotify(this.$vToastify, error.response.data);
      }
   }).finally(() => {
      // this.$vToastify.stopLoader();
   });
};


export async function apiTag(data = {}, type = 'create', method = 'post', id = null) {
   this.$vToastify.loader("Подождите, обновляем данные", "Профиль");
   return this.$axios({
      method: method,
      url: `business/menu/tag/${type}`,
      data: data,
      params: { menu_id: id }
   }).then(res => {
      data = {};
      this.$vToastify.success("Данные успешно применены", "Тэг");
      return res.data;
   }).catch(error => {
      if (error.response) {
         catchNotify(this.$vToastify, error.response.data);
      }
   }).finally(() => {
      this.$vToastify.stopLoader();
   });
};




