export function catchNotify(vToastify, data) {
   if (data.errors) {
      setTimeout(() => {
         let k = 0;
         Object.keys(data.errors).forEach((item) => {
            let err = data.errors[item];
            if (k > 2) {
               return false;
            }
            k++;
            for (let i = 0; i < err.length; i++) {
               setTimeout(() => {
                  vToastify.error(err[i], "Ошибка")
               }, 100 * k);
            }
          
         });
      }, 600);
   }
   else if (data.message) {
      vToastify.error(data.message, "Ошибка")
   }
};