const apiLocale = (axios) => {
   return axios({
      method: 'get',
      url: `project/localizations`
   }).then(res => {
      return res.data;
   }).catch(() => {
      return null;
   })
}

export default apiLocale