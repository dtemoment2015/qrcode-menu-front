const apiMenu = (axios, params = {}, type = 'list') => {
   return axios({
      method: 'get',
      params: params,
      url: `business/menu/${type}`
   }).then(res => {
      return res.data;
   }).catch(() => {
      if (type === 'list') {
         return [];
      }
      return null;
   })
}

export default apiMenu