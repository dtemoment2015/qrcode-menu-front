const apiAccount = (axios, store, cookies) => {
   return axios({
      method: 'get',
      url: `business`
   }).then(res => {
      return res.data;
   }).catch(() => {
      cookies.remove('token');
      axios.setToken(false);
      store.dispatch('initProfile', null);
   })
}

export default apiAccount