const apiMenuAnalytic = (axios, params = {}, type = 'list') => {
   return axios({
      method: 'get',
      params: params,
      url: `business/menu/analytic/list`
   }).then(res => {
      return res.data;
   }).catch(() => {
      return null;
   })
}

export default apiMenuAnalytic