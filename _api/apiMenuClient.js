const apiMenuClient = (axios, params = {},redirect) => {
   console.log(params);
   return axios({
      method: 'get',
      params: params,
      url: `client/menu`
   }).then(res => {
      return res.data;
   }).catch(() => {
      redirect('/');
      return null;
   })
}

export default apiMenuClient